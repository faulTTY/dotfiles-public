# FaulTTY's dotfiles

**Warning**: Don’t blindly use my settings unless you know what that entails. Use at your own risk!

## Contents

- bash config
- fish config
- kitty config
- alacritty config
- starship config

## Fish shell setup

- [Fish shell](https://fishshell.com/)
- [Shellder](https://github.com/simnalamburt/shellder) - Shell theme
- [Nerd fonts](https://github.com/ryanoasis/nerd-fonts) - Powerline-patched fonts
- [z for fish](https://github.com/jethrokuan/z) - Directory jumping
- [Exa](https://the.exa.website/) - `ls` replacement

## About me

I'm just a stranger.
