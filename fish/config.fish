starship init fish | source
alias l "exa -l -g --icons"
alias lt "exa -T --level 3 --icons"
alias ll "exa -la --icons"
alias lta "exa -Ta --level 3 --icons"
